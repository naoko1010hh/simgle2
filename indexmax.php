<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@naoko1010hh">
<meta property="og:type" content="website">
<meta property="og:site_name" contet="simgle.0u0.biz">
<meta property="og:locale" content="ja_JP">
<meta property="og:image" content="https://simgle.0u0.biz/150.png">
<link rel="apple-touch-icon-precomposed" href="https://simgle.0u0.biz/150.png">
<meta name="msapplication-TileImage" content="https://simgle.0u0.biz/150.png">
<meta name="msapplication-TileColor" content="#555">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="./16.png" sizes="16x16" type="image/png">
<link rel="icon" href="./32.png" sizes="32x32" type="image/png">
<link rel="icon" href="./48.png" sizes="48x48" type="image/png">
<link rel="icon" href="./64.png" sizes="64x64" type="image/png">
<title>Simgle.0u0.biz シンプルなグーグル検索 軽く素早く使いやすく</title>
<meta property="og:title" content="Simgle.0u0.biz シンプルなグーグル検索 軽く素早く使いやすく">
<meta name="description" content="Simgle 0u0.biz シンプルなグーグル検索軽くて使いやすいユーザーの意見を最優先に改良していきます。ショートカットカスタマイズで機能で自分の好きなようなデザインへ くもことなおこ naoko1010hh">
<meta property="og:description" content="Simgle 0u0.biz シンプルなグーグル検索軽くて使いやすいユーザーの意見を最優先に改良していきます。ショートカットカスタマイズで機能で自分の好きなようなデザインへ くもことなおこ naoko1010hh">
<meta property="og:url" content="https://simgle.0u0.biz/">
<link rel="canonical" href="https://simgle.0u0.biz/">
<meta name="google" content="notranslate">
<link rel="stylesheet" href="./m.css">
<style media="screen">
abbr,address,article,aside,audio,b,blockquote,body,canvas,caption,cite,code,dd,del,details,dfn,div,dl,dt,em,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,p,pre,q,samp,section,small,span,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,ul,var,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent}body{line-height:1}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}nav ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent}ins{text-decoration:none}ins,mark{background-color:#ff9;color:#000}mark{font-style:italic;font-weight:700}del{text-decoration:line-through}abbr[title],dfn[title]{border-bottom:1px dotted;cursor:help}table{border-collapse:collapse;border-spacing:0}hr{display:block;height:1px;border:0;border-top:1px solid #ccc;margin:1em 0;padding:0}input,select{vertical-align:middle}


a{color:#999}
#night-bin:checked ~ #back{
background-color:#222;
mix-blend-mode:difference;
position:absolute
}
#night-bin:checked ~ .short li a{
background-color:#fff
}
#night-bin:checked ~ .short li a img{
background-color:#000
}
#night-bin:checked ~ #g-icon .b{
display:none
}
#light-bin:checked ~ #g-icon .w{
display:none
}
#night-bin:checked ~ #night-icon{display:none}
#light-bin:checked ~ #light-icon{display:none}
#night-bin:checked ~ footer{background-color:#111;color:#fff}

#night-bin,
#light-bin{
display:none
}

#light-icon{
background-color:#fff
}
#night-icon{
background-color:#000
}
#night-icon,#light-icon{
display:block;
box-sizing:border-box;
width:48px;
height:48px;
position:fixed;
z-index:5;
top:16px;
right:16px;
cursor:pointer;
border-radius:24px
}
#night-icon img,#light-icon img{
width:100%;
image-rendering:pixelated;
image-rendering:-webkit-crisp-edges;
image-rendering:-moz-crisp-edges;
-ms-interpolation-mode:nearest-neighbor
}
#back{
background-color:#eee;
display:block;
width:100vw;
height:100vh;
position:fixed;
z-index:0;
transition:.3s
}
#g-icon{
text-align:center;
position:fixed;
z-index:4;
top:22vh;
left:0;
right:0;
margin:auto
}
#g-icon img{
height:25vh;
max-height:150px;
image-rendering:pixelated;
image-rendering:-webkit-crisp-edges;
image-rendering:-moz-crisp-edges;
-ms-interpolation-mode:nearest-neighbor
}


.short{
display:inline-block;
margin:10px;
font-size:0;
position:fixed;
z-index:5
}
.short.left-w,.short.left-h{
left:0;
top:0
}
.short.left-w li{ display:inline-block; }
.short.left-h li{ display:block; }
.short li{
margin:6px
}
.short li a{
display:block;
box-sizing:border-box;
width:48px;
height:48px;
padding:3px;
background-color:#000
}
.short li a img{
display:block;
height:36px;
width:36px;
padding:3px;
background-color:#eee
}

#search{
position:fixed;
z-index:4;
top:50vh;
left:0;
right:0;
margin:auto
}
#search input{
width:70vw;
height:40px;
padding:0 16px;
margin:0 auto;
font-size:20px;
border:1px #ddd solid;
border-radius:20px;
outline:none;
-webkit-appearance:none;
-moz-appearance:none;
appearance:none;
position:fixed;
left:0;
right:0
}

footer{
background-color:#fff;
display:block;
width:100%;
padding:10px 0;
text-align:center;
position:fixed;
bottom:0
}
footer p,footer small{
font-size:19px
}
</style>
</head>
<body onload="document.search.q.focus()">
<input type="radio" id="night-bin" name="ln-exchange" <?php echo isset($_GET['d']) ? "checked":""; ?>>
<input type="radio" id="light-bin" name="ln-exchange" <?php echo isset($_GET['d']) ? "":"checked"; ?>>
<label id="night-icon" for="night-bin"><img src="data:image/gif;base64,R0lGODlhEAAQAIAAAP///////yH5BAEKAAEALAAAAAAQABAAAAIdjI+pywcPWoBP0tguZldO6nUWOJKZeUKega4uUwAAOw==" alt=""></label>
<label id="light-icon" for="light-bin"><img src="data:image/gif;base64,R0lGODlhEAAQAIABAAAAAP///yH5BAEKAAEALAAAAAAQABAAAAIkjI+pywfQwjNTwlRRVm+v3n0QOG0laYlgZHna9XJX5cbsjRsFADs=" alt=""></label>
<div id="back"></div>
<h1 id="g-icon">
<img class="w" src="data:image/gif;base64,R0lGODlhBgAHAIAAAP///////yH+EUNyZWF0ZWQgd2l0aCBHSU1QACH5BAEKAAEALAAAAAAGAAcAAAIKDA6WuqEOG5IsFQA7" alt="googleGsimgle">
<img class="b" src="data:image/gif;base64,R0lGODlhBgAHAIABAAAAAP///yH+EUNyZWF0ZWQgd2l0aCBHSU1QACH5BAEKAAEALAAAAAAGAAcAAAIKDA6WuqEOG5IsFQA7" alt="googleGsimgle">
</h1>
<ul class="short <?php echo isset($_GET['sw']) ? "left-w":"left-h"; ?>">
<?php
function h($s){return htmlentities($s,ENT_QUOTES,'UTF-8');}
if(isset($_GET["u"])){
$black=isset($_GET['b']) ? 'target="_blank"':'';
foreach(explode(',',h($_GET["u"]))as$v){
echo '<li><a href="'.$v.'" '.$black.'><img src="https://www.google.com/s2/favicons?domain='.$v.'" alt=""></a></li>';
}}
?>
</ul>
<form id="search" method="get" action="https://www.google.com/search" name="search" autocomplete="off"><input type="search" class="search" name="q" value="" x-webkit-speech="" id="sbi"><input type="hidden" name="ie" value="UTF-8"><input type="hidden" name="sa" value="Search"><input type="hidden" name="channel" value="fe"><input type="hidden" name="client" value="browser-ubuntu"><input title="search" type="hidden" name="hl" value="ja"></form>
<footer><p>Create by <a href="">naoko1010hh</a></p><small>&copy; 2019 Simgle</small></footer>
</body>
</html>
